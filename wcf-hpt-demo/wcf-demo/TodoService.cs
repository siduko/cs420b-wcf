﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace wcf_demo
{
    public class TodoService : ITodoService
    {
        databaseEntities todos = new databaseEntities();

        private UserModel getUserModel(users user)
        {
            UserModel userModel = new UserModel();
            userModel.UserId = user.userid;
            userModel.UserName = user.username;
            userModel.Email = user.email;
            userModel.Address = user.address;
            userModel.Phone = user.phone;
            return userModel;
        }

        private users getUsers(UserModel userModel){
            users user = new users();
            user.userid = userModel.UserId;
            user.username = userModel.UserName;
            user.password = userModel.Password;
            user.email = userModel.Email;
            user.address = userModel.Address;
            user.phone = userModel.Phone;
            return user;
        }

        public UserModel login(string userName, string password)
        {
            var userEntity = todos.users.FirstOrDefault(u => u.username == userName && u.password == password);
            UserModel user = null;
            if (userEntity != null)
            {
                user = getUserModel(userEntity);
            }
            return user;
        }

        public UserModel register(UserModel userModel)
        {
            users user = todos.users.Add(getUsers(userModel));
            todos.SaveChanges();

            return getUserModel(user);
        }

        public UserModel updateUser(UserModel userModel)
        {
            users user = todos.users.FirstOrDefault(u => u.userid == userModel.UserId);
            user.username = userModel.UserName;
            user.password = userModel.Password;
            user.email = userModel.Email;
            user.address = userModel.Address;
            user.phone = userModel.Phone;
            todos.SaveChanges();

            return getUserModel(user);
        }

        public bool addTodo(TodoModel todoModel)
        {
            throw new NotImplementedException();
        }

        public bool removeTodo(string id)
        {
            throw new NotImplementedException();
        }

        public bool updateTodo(TodoModel todoModel)
        {
            throw new NotImplementedException();
        }

        public List<TodoModel> getAllTodoByUser(string userId)
        {
            throw new NotImplementedException();
        }
    }
}
