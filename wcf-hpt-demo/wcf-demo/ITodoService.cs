﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace wcf_demo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ITodoService
    {
        [WebInvoke(Method="GET",UriTemplate = "login?username={userName}&password={password}", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        UserModel login(string userName, string password);

        [WebInvoke(Method = "POST", UriTemplate = "register", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        UserModel register(UserModel userModel);

        [WebInvoke(Method = "PUT", UriTemplate = "user/update", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        UserModel updateUser(UserModel userModel);

        [WebInvoke(Method = "POST", UriTemplate = "todo/add", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        bool addTodo(TodoModel todoModel);

        [WebInvoke(Method = "DELETE", UriTemplate = "todo/delete/{id}", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        bool removeTodo(string id);

        [WebInvoke(Method = "PUT", UriTemplate = "todo/update", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [OperationContract]
        bool updateTodo(TodoModel todoModel);

        [WebGet(UriTemplate = "todo?userId={userId}", ResponseFormat = WebMessageFormat.Json)]
        [OperationContract]
        List<TodoModel> getAllTodoByUser(string userId);

    }

    [DataContract]
    public class UserModel
    {
        [DataMember]
        public string UserId { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string Phone { get; set; }

    }
    [DataContract]
    public class TodoModel
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Contant { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }

        [DataMember]
        public string UserId { get; set; }
    }
}
