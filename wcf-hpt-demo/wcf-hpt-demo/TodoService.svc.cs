﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace wcf_hpt_demo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TodoService : ITodoService
    {
        todoEntities todos = new todoEntities();

        private UserModel getUserModel(users user)
        {
            UserModel userModel = new UserModel();
            userModel.UserId = user.userid;
            userModel.UserName = user.username;
            userModel.Email = user.email;
            userModel.Address = user.address;
            return userModel;
        }

        public UserModel login(string userName,string password)
        {
            var userEntity = todos.users.FirstOrDefault(u => u.username == userName && u.password == password);
            UserModel user = null;
            if (userEntity != null)
            {
                user = getUserModel(userEntity);
            }
            return user;
        }

        public UserModel register(UserModel userModel)
        {
            throw new NotImplementedException();
        }

        public UserModel updateUser(UserModel userModel)
        {
            throw new NotImplementedException();
        }

        public bool addTodo(TodoModel todoModel)
        {
            throw new NotImplementedException();
        }

        public bool removeTodo(string id)
        {
            throw new NotImplementedException();
        }

        public bool updateTodo(TodoModel todoModel)
        {
            throw new NotImplementedException();
        }

        public List<TodoModel> getAllTodoByUser(string userId)
        {
            throw new NotImplementedException();
        }
    }
}
